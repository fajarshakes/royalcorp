-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 12, 2020 at 07:00 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chatbot`
--

-- --------------------------------------------------------

--
-- Table structure for table `monitor_rto`
--

CREATE TABLE `monitor_rto` (
  `ID` int(11) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `nama_unit` varchar(30) NOT NULL,
  `tanggal_rto` date NOT NULL,
  `jam_rto` time NOT NULL,
  `tanggal_reply` date NOT NULL,
  `jam_reply` time NOT NULL,
  `durasi` int(10) NOT NULL,
  `keterangan` varchar(150) NOT NULL,
  `menit_ke` int(2) NOT NULL,
  `email` int(2) NOT NULL,
  `tanggal_rto2` datetime DEFAULT NULL,
  `tanggal_reply2` datetime DEFAULT NULL,
  `durasi2` int(11) DEFAULT NULL,
  `diabaikan` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `monitor_rto`
--
ALTER TABLE `monitor_rto`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `monitor_rto`
--
ALTER TABLE `monitor_rto`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
