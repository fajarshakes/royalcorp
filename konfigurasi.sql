-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 12, 2020 at 06:59 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chatbot`
--

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasi`
--

CREATE TABLE `konfigurasi` (
  `nama_sti` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `token_telegram` varchar(50) DEFAULT NULL,
  `id_chat` varchar(50) DEFAULT NULL,
  `proxy` varchar(50) DEFAULT NULL,
  `ip_server` varchar(50) DEFAULT NULL,
  `ip_public` varchar(50) DEFAULT NULL,
  `host_smtp` varchar(50) NOT NULL,
  `port_smtp` varchar(50) NOT NULL,
  `username_smtp` varchar(50) NOT NULL,
  `password_smtp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfigurasi`
--

INSERT INTO `konfigurasi` (`nama_sti`, `email`, `token_telegram`, `id_chat`, `proxy`, `ip_server`, `ip_public`, `host_smtp`, `port_smtp`, `username_smtp`, `password_smtp`) VALUES
('WAN PLN Aceh', 'iqbal.nk@pln.co.id', '701126316:AAFzmoT0tmy49hqVXLCWC4XnPztLU_2HsTA', '-1001140762562', '', '10.15.1.181', '103.3.79.84:7080', 'hub.pln.co.id', '25', 'iqbal.nk', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  ADD UNIQUE KEY `nama_sti` (`nama_sti`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
