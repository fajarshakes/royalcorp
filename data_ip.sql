-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 12, 2020 at 06:59 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chatbot`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_ip`
--

CREATE TABLE `data_ip` (
  `ID` int(2) NOT NULL,
  `ip_address` varchar(128) NOT NULL,
  `nama_unit` varchar(128) NOT NULL,
  `jam_rto` time DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'online',
  `kategori` int(2) NOT NULL DEFAULT 0,
  `tanggal_rto` datetime DEFAULT NULL,
  `tanggal_reply` datetime DEFAULT NULL,
  `tanggal_email` datetime DEFAULT NULL,
  `email_pic` varchar(200) DEFAULT NULL,
  `email_manajemen` varchar(200) DEFAULT NULL,
  `email_os` varchar(200) DEFAULT NULL,
  `layer` varchar(200) DEFAULT NULL,
  `dipindai` int(2) DEFAULT 1,
  `sla` decimal(10,2) DEFAULT 1.00,
  `service_id` varchar(50) DEFAULT NULL,
  `tracert` longblob DEFAULT NULL,
  `inisial` varchar(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_ip`
--
ALTER TABLE `data_ip`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_ip`
--
ALTER TABLE `data_ip`
  MODIFY `ID` int(2) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
